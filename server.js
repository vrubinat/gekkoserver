var express = require('express');
var utils = require('./src/gecko.appUtils')(); 
var path = require('path');

var gecko_logger = require('./src/gecko.logger');

var logger = gecko_logger.log;

// Routes
var r_data = require('./src/routes/data');


app = express();


// Configurations for all environments
app.configure(function() {
	
	app.set('title', 'Gecko');

	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(utils.traceRequest);
	app.use(utils.logErrors);
	app.use(utils.clientErrorHandler);


	app.use('/public',express.static(path.join(__dirname, 'www')));


	// app.all('/prv/*', utils.cors);
	app.all('*', utils.cors);
	
});

// development only
app.configure('development', function() {
	logger.info("Development environment");
	//Url Couch DB
	//app.set('couchdb_data','https://eds.cloudant.com/gecko');
	
	app.set('couchdb_data','https://aure.iriscouch.com:6984/gecko/');
});

// production only
app.configure('production', function() {
	logger.info("Production environment");
	//Url Couch DB
	app.set('couchdb_data','https://aure.iriscouch.com:6984/gecko/');
	
	
});


/*
 *  App routes
 */ 


//Data
app.get('/items', r_data.data);
app.get('/items/:id', r_data.data);
app.del('/delete/:id', r_data.remove);
app.put('/insert', r_data.insert);
app.put('/update', r_data.update);



// Start App

logger.info("Starting Server.....");
app.listen(9091);
