var gecko_logger = require('./gecko.logger');
var trace = gecko_logger.trace;
var error = gecko_logger.error;
var logger = gecko_logger.log;

exports = module.exports = createUtils;


function createUtils() {
	return new Utils();
}

function Utils() {

};


Utils.prototype.logErrors = function(err, req, res, next) {
	error.error(err.stack);
	logger.error(err.stack);
	next(err);
};

Utils.prototype.traceRequest = function(req, res, next) {
	trace.info(req.method, ' : ', req.url);
	next();
};

Utils.prototype.clientErrorHandler = function(err, req, res, next) {
	if (req.xhr) {
		res.send(500, {
			error: 'service_error'
		});
	} else {
		next(err);
	}
};

Utils.prototype.cors = function(req, res, next) {

	logger.info("CORS: " + req.headers);

	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Methods","POST, GET, PUT, DELETE, OPTIONS");
	res.header("Access-Control-Allow-Credentials",true);
	res.header("Access-Control-Max-Age",'86400'); // 24 hours
	res.header("Access-Control-Allow-Headers","X-Requested-With , Content-Type ");
	if (req.method === 'OPTIONS') {
		res.end();
	}else{
		next();
	}
	


};

