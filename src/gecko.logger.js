
var log4js = require('log4js');

log4js.loadAppender('file');
//log4js.addAppender(log4js.appenders.console());
log4js.addAppender(log4js.appenders.file('logs/server.log'), 'server');
log4js.addAppender(log4js.appenders.file('logs/trace.log'), 'trace');
log4js.addAppender(log4js.appenders.file('logs/error.log'), 'error');

var logger = log4js.getLogger('server');
var trace = log4js.getLogger('trace');
var error = log4js.getLogger('error');

logger.setLevel('DEBUG');
trace.setLevel('INFO');
error.setLevel('INFO');

var logentries = require('node-logentries');
var log = logentries.logger({
  token:process.env.LOGENTRIES_TOKEN
});

// exports logger for use in app
//module.exports.log = logger;
//module.exports.trace = trace;
//module.exports.error = error;

module.exports.log = log;
module.exports.trace = log;
module.exports.error = log;

