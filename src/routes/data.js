var gecko_logger = require('../gecko.logger');
var error = gecko_logger.error;
var logger = gecko_logger.log;

var login = function(callback) {

	return require('nano')(app.get('couchdb_data'));

};

var sendData = function(res, qry, data) {
	var callback = qry.callback;
	if (callback) {
		res.jsonp(data); //callback + '('+ JSON.stringify(data) + ');');
	} else {
		res.json(data);
	}
}

var data = function(req, res) {
	var couch = login();
	var id = req.params.id;
	var qry = req.query;

	if (id) {
		couch.get(id, function(err, data) {
			if (!err) {
				sendData(res, qry, data);
			} else {
				res.end(500);
			}
		});
	} else {

		var param = {};
		if (qry.offset) {
			param.skip = qry.offset;
		}
		if (qry.count) {
			param.limit = qry.count;
		}
		couch.view('views', 'data', param, function(err, body, headers) {
			if (!err) {
				var data = [];
				body.rows.forEach(function(doc) {
					data.push(doc.value);
				});

				sendData(res, qry, data);
			} else {
				res.end(500);
			}
		});

	}
};

var remove = function(req, res) {

	var id = req.params.id;
	var couch = login();

	couch.get(id, function(err, data) {
		if (!err) {
			couch.destroy(id, data._rev);
			res.json({
				ok: true
			});
		} else {
			res.end();
		}
	});

};

var insert = function(req, res) {
	var data = req.body;
	var couch = login();
	couch.insert(data, data.id, function(err, body) {
		if (!err) {
			res.json({
				insert: true
			});
		} else {
			res.json({
				insert: false
			});
		}
	});
};

var update = function(req, res) {
	var data = req.body;
	var couch = login();
	couch.insert(data, data.id, function(err, body) {
		if (err) {
			data._rev = body._rev;
			couch.insert(data, data.id, function(err, body) {
				if (!err) {
					res.json({
						update: true
					});
				}else{
					res.json({
						update: false
					});
				}
			});
		} else {
			res.json({
				insert: true
			});
		}
	});
};

module.exports.data = data;
module.exports.remove = remove;
module.exports.insert = insert;
module.exports.update = update;