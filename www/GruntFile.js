module.exports = function(grunt) {

  grunt.loadNpmTasks('grunt-shell');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-less');

  // Do grunt-related things in here
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    shell: {
      multiple: {
        command: 'python build.py -v 0.1',
        options: {
          stdout: true,
          execOptions: {
            cwd: 'build'
          }
        }

      }
    },
    jshint: {
      // You get to make the name
      // The paths tell JSHint which files to validate
      options: {
        sub: true
      },
      myFiles: ['src/**/*.js']
    },
    less: {
      production: {
        options: {
          yuicompress: true
        },
        files: {
          "bin/gecko.min.0.2.css": "less/gecko.less"
        }
      }
    }
  });

  grunt.registerTask('default', ['shell','less','jshint']);
  grunt.registerTask('valid', ['jshint']);
  grunt.registerTask('css', ['less']);
};