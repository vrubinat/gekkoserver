goog.provide('gecko.controllers.PouchDetailController');
              
goog.require('gecko.conectors.pouchDB');

/**
 * [PouchDetailController description]
 * @param {Object} $scope       [description]
 * @param {Object} $routeParams [description]
 * @constructor
 */
gecko.controllers.PouchDetailController = function($scope, $routeParams) {
	$scope['bookView'] = false;

	var func = function(id) {
		var db = gecko.conectors.pouchDB.getInstance().getDB();
		db.get(id, function(err, doc) {
			$scope['book'] = doc;
			$scope['bookView'] = true;
			$scope.$apply();
		});

	};

	$scope.$watch('element', function() {
		if ($scope['element']) {
			func($scope['element']);
		}

	});


	if ($routeParams['bookId']) {
		func($routeParams['bookId']);

	}

};