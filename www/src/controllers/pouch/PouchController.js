goog.provide('gecko.controllers.PouchController');

goog.require('gecko.controllers.PouchListController');
goog.require('gecko.controllers.PouchDetailController');
goog.require('gecko.controllers.PouchNewController');
goog.require('gecko.controllers.PouchEditController');

goog.require('gecko.conectors.pouchDB');

/**
 * Function to create the module PouchController
 * and init the pouchDB Database
 */
gecko.controllers.PouchController = function() {

	gecko.conectors.pouchDB.getInstance().init();

	var module = angular.module('PouchController',[]);

	//asign Controlers to App scope

	module.controller('geckoListCtrl', ['$scope', gecko.controllers.PouchListController]);

	module.controller('createCtrl', ['$scope', '$location', gecko.controllers.PouchNewController]);

	module.controller('editCtrl', ['$scope', '$location', '$routeParams', gecko.controllers.PouchEditController]);

	module.controller('geckoDetailCtrl', ['$scope', '$routeParams', gecko.controllers.PouchDetailController]);

};