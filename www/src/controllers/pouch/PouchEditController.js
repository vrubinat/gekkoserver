goog.provide('gecko.controllers.PouchEditController');

goog.require('gecko.conectors.pouchDB');

/**
 * [PouchEditController description]
 * @param {Object} $scope    [description]
 * @param {Object} $location [description]
 * @param {Object} $params [description]
 * @constructor
 */
gecko.controllers.PouchEditController = function($scope, $location, $params) {

	var db = gecko.conectors.pouchDB.getInstance().getDB();

	if ($params && $params['bookId']) {
		db.get($params['bookId'], function(err, doc) {
			if (!err) {
				$scope['gecko'] = doc;
				$scope.$apply();
			}
		});
	}

	$scope['save'] = function(data) {
		//console.log(this.gecko);
		db.put(this['gecko'], function(err, response) {
			$location.path('/');
			this.location.hash = "#"; 
		});

	};


};