goog.provide('gecko.controllers.BookListController');

goog.require('goog.array');

/**
 * [BookListController description]
 * @param {Object} $scope [description]
 * @param {Object} $http  [description]
 * @constructor
 */
gecko.controllers.BookListController = function($scope, $http) {
	
	$scope['go'] = goog.bind(this.go,this,$scope);
	$scope['delete'] = goog.bind(this.del,this,$scope,$http);

	$http.jsonp(geckoApp.url+'items?callback=JSON_CALLBACK').success(function(data) {
		$scope['books'] = data;
	});

};

gecko.controllers.BookListController.prototype.go = function($scope,data) {
	$scope['element'] = data;
};

gecko.controllers.BookListController.prototype.del = function($scope,$http,data) {
	
	
	var books = goog.array.filter($scope['books'], function(obj){
		if (data === obj['id']){
			return false;
		}else{
			return true;
		}
	}); 
	
	$http['delete'](geckoApp.url+'delete/'+data);
	$scope['books'] = books;

};
