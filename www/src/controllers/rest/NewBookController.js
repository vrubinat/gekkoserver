goog.provide('gecko.controllers.NewBooksController');

/**
 * [NewBooksController description]
 * @param {Object} $scope    [description]
 * @param {Object} $http     [description]
 * @param {Object} $location [description]
 * @constructor
 */
gecko.controllers.NewBooksController = function($scope, $http, $location,$params) {


	$scope['save'] = function(data) {
		$http.put(geckoApp.url+'insert', this['gecko']).success(function(data) {
			$location.path('/');	
		});
		
	};


};