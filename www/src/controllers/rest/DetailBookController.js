goog.provide('gecko.controllers.DetailBookController');



/**
 * [DetailBookController description]
 * @param {Object} $scope       [description]
 * @param {Object} $http        [description]
 * @param {Object} $routeParams [description]
 * @constructor
 */
gecko.controllers.DetailBookController = function($scope, $http, $routeParams) {
	$scope['bookView'] = false;

	var func = function(id) {
		$http.jsonp(geckoApp.url+'items/' + id + '/?callback=JSON_CALLBACK').success(function(data) {

			$scope['book'] = data;
			$scope['bookView'] = true;

		});
	};

	$scope.$watch('element', function() {
		if ($scope['element']) {
			func($scope['element']);
		}

	});


	if ($routeParams['bookId']) {
		func($routeParams['bookId']);

	}

};