goog.provide('gecko.controllers.PhoneGapDetailController');


/**
 * [PhoneGapDetailController description]
 * @param {Object} $scope       [description]
 * @param {Object} $routeParams [description]
 * @constructor
 */
gecko.controllers.PhoneGapDetailController = function($scope, $routeParams) {
	$scope['bookView'] = false;

	var func = function(id) {

		cordova.exec(function(doc) {
				$scope['book'] = doc;
				$scope['bookView'] = true;
				$scope.$apply();
			}, function(error) {}, "DbPlugin",
			"get", [id]);

	};

	$scope.$watch('element', function() {
		if ($scope['element']) {
			func($scope['element']);
		}

	});

	if ($routeParams['bookId']) {
		func($routeParams['bookId']);

	}

};