goog.provide('gecko.controllers.PhoneGapListController');

goog.require('goog.array');


/**
 * [PhoneGapListController description]
 * @param {Object} $scope [description]
 * @constructor
 */
gecko.controllers.PhoneGapListController = function($scope) {

	$scope['go'] = goog.bind(this.go, this, $scope);
	$scope['delete'] = goog.bind(this.del, this, $scope);

	var f = goog.bind(this.list, this, $scope);

	cordova.exec(f, function(error) {}, "DbPlugin",
		"getAll", []);

	window['updateData'] = function(){
		cordova.exec(f, function(error) {}, "DbPlugin",
			"getAll", []);	
	};

};

gecko.controllers.PhoneGapListController.prototype.list = function($scope, docs) {
	var data = [];

	goog.array.forEach(docs, function(doc) {
		if (doc['_id'] !== "_design/views") {
			doc['id'] = doc['_id'];
			data.push(doc);
		}

	});
	$scope['books'] = data;
	$scope.$apply();

};

gecko.controllers.PhoneGapListController.prototype.go = function($scope, data) {
	$scope['element'] = data;
};

gecko.controllers.PhoneGapListController.prototype.del = function($scope, data) {


	var books = goog.array.filter($scope['books'], function(obj) {
		if (data === obj['id']) {
			return false;
		} else {
			return true;
		}
	});

	cordova.exec(function(d) {
			$scope['books'] = books;
			$scope.$apply();
		}, function(error) {}, "DbPlugin",
		"delete", [data]);


};