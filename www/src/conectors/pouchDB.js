goog.provide('gecko.conectors.pouchDB');
goog.provide('gecko.conectors.eventType');

goog.require('goog.events');
goog.require('goog.events.EventTarget');
goog.require('goog.events.EventWrapper');
goog.require('goog.events.EventHandler');
goog.require('goog.debug.ErrorHandler');
goog.require('goog.array');



gecko.conectors.eventType = {
	CHANGE: 'change'
};

/** 
 * Singleton Class for connect with pouchDB
 * @extends {goog.events.EventTarget}
 * @constructor
 */
gecko.conectors.pouchDB = function() {
	goog.base(this);
};

goog.inherits(gecko.conectors.pouchDB, goog.events.EventTarget);

goog.addSingletonGetter(gecko.conectors.pouchDB);

/**
 * @type {PouchDB}
 */
gecko.conectors.pouchDB.prototype.DB_;

/**
 * Function to create PouchDB and start the replication
 *  the external db is define in geckoApp.db
 */
gecko.conectors.pouchDB.prototype.init = function() {
	this.DB_ = new PouchDB('gecko');

	var opts = {
		'continuous': true,
		'conflicts': true,
		'onChange': goog.bind(this.onChange_, this)
	};

	this.DB_.replicate.to(geckoApp.db, opts);
	this.DB_.replicate.from(geckoApp.db, opts);

};

/**
 * Return pouchdb object
 * @return {Object} 
 */
gecko.conectors.pouchDB.prototype.getDB = function() {
	return this.DB_;
};

/**
 * [onChange_ description]
 * @param  {Object} changes [description]
 * @private
 */
gecko.conectors.pouchDB.prototype.onChange_ = function(changes) {

	if (changes['docs_read'] && changes['docs_read'] > 0) {
		this.dispatchEvent(gecko.conectors.eventType.CHANGE);
	}
	if (!changes['_conflicts']) return;

	goog.array.forEach(changes['_conflicts'], function(doc) {
		//TODO: Detect and solve conflicts
	});



};