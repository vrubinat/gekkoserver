goog.provide('gecko');
goog.provide('geckoApp');

goog.require('gecko.controllers.UrlController');
goog.require('gecko.controllers.PouchController');
goog.require('gecko.controllers.PhoneGapController');
goog.require('goog.userAgent');



/**
 * Application for view books
 * It's possible use different controllers for get data
 * from different source.
 *
 * The actual options are:
 * - UrlController
 * - PouchController
 * - PhoneGapController
 *
 * The PouchController and PhoneGapController used
 * continuos changes detection for update data in view.
 *
 * @param {string=} opt_type Controller type
 * @param {Object=} opt_options options for controller
 * @constructor
 */
geckoApp = function(opt_type, opt_options) {

  // Detect optional's parameters
  if (!opt_type) opt_type = 'PouchController';

  if (opt_options) {
    if (opt_options['url']) geckoApp.url = opt_options['url'];
    if (opt_options['db']) geckoApp.db = opt_options['db'];
  }

  // Detect IE
  // IE explorer not support pouchDB except IE 10
  if (goog.userAgent.IE && !goog.userAgent.isVersionOrHigher(10)){
    opt_type='UrlController';
  }

  // init Controllers
  var init = {
    'PouchController': gecko.controllers.PouchController,
    'PhoneGapController': gecko.controllers.PhoneGapController,
    'UrlController': gecko.controllers.UrlController
  };

  init[opt_type]();


  this.app_ = angular.module('gecko', ['ngRoute', opt_type]);


  this.initRoute_();

  this.initDirective_();

  // init angular 
  angular.bootstrap(document, ['gecko']);

};

/**
 * @type {angular.Module}
 */
geckoApp.prototype.app_;

/**
 * Function to create Routes for App
 * @private
 */
geckoApp.prototype.initRoute_ = function() {
  // Config Routes 

  this.app_.config(['$routeProvider',
    function($routeProvider) {
      $routeProvider.
      when('/', {
        controller: 'geckoListCtrl',
        templateUrl: 'views/list.html'
      }).
      when('/detail/:bookId', {
        controller: 'geckoDetailCtrl',
        templateUrl: 'views/detail.html'
      }).
      when('/new', {
        controller: 'createCtrl',
        templateUrl: 'views/new.html'
      }).
      when('/edit/:bookId', {
        controller: 'editCtrl',
        templateUrl: 'views/new.html'
      }).
      otherwise({
        redirectTo: '/'
      });
    }
  ]);

};

/**
 * function to create a Directives for app
 * @private
 */
geckoApp.prototype.initDirective_ = function() {
  // Create Directives

  this.app_.directive('mySubArea', function() {
    return {
      'restrict': 'E',
      'templateUrl': 'views/detail.html',
      'controller': 'geckoDetailCtrl'
    };
  });

  var FLOAT_REGEXP = /^\-?\d+((\.|\,)\d+)?$/;
  this.app_.directive('smartFloat', function() {
    return {
      'require': 'ngModel',
      'link': function(scope, elm, attrs, ctrl) {
        ctrl.$parsers.unshift(function(viewValue) {
          if (FLOAT_REGEXP.test(viewValue)) {
            ctrl.$setValidity('float', true);
            return parseFloat(viewValue.replace(',', '.'));
          } else {
            ctrl.$setValidity('float', false);
            return undefined;
          }
        });
      }
    };
  });
};
/**
 * [url description]
 * @type {string}
 */
geckoApp.url = "http://gecko.eu01.aws.af.cm/";

/**
 * [db description]
 * @type {string}
 */

geckoApp.db = "https://aure.iriscouch.com:6984/gecko/";

goog.exportSymbol('geckoApp', geckoApp);